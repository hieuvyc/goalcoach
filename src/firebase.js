import * as firebase from 'firebase';

const config = {
   apiKey: "AIzaSyAwKm5LHI1bqjo-PP2B1XKPRzkiJG5auJk",
   authDomain: "gocoach-d99d7.firebaseapp.com",
   databaseURL: "https://gocoach-d99d7.firebaseio.com",
   projectId: "gocoach-d99d7",
   storageBucket: "",
   messagingSenderId: "66782111540"
 };

 export const firebaseApp = firebase.initializeApp(config);
 export const goalRef = firebase.database().ref('goals');
 export const completeGoalRef = firebase.database().ref('completeGoals');
